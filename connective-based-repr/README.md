# README #

This repository contains the word representations used in the paper:

Learning Connective-based Word Representations for Implicit Discourse Relation Identification, Chloé Braud and Pascal Denis, in Proceedings of EMNLP, 2016

Real valued vectors representing words in English have been built based on their presence in the context of a discourse connective in the Bllip dataset automatically annotated with connectives.

### Data ###

* Two versions based on a TF IDF or a PPMI IDF normalization
* Only the original embeddings are provided (96 dimensions), lower dimensional versions are tested in the paper (using PCA), but leading to lower results.


### Use ###

* The files bllip-tok-pmiidf_embeddings and bllip-tok-tfidf_embeddings contain one vector per line.
* The files bllip-tok-pmiidf_vocab and bllip-tok-tfidf_vocab contain one word per line.


For any question, please contact: chloe.braud@gmail.com